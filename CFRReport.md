# CageFightR: clusters CTSSs across all samples to create a consensus set of TCs. 

## Counting and Pre-filtering

First, we calculate the support, TPM, and pooled CTSSs.

```R
# Calculate support, TPM, and pooled CTSSs
nextCTSSs <- calcSupport(nextCTSSs, inputAssay="counts", outputColumn="support", unexpressed=0)
supportedCTSSs <- subset(nextCTSSs, support > 1)
supportedCTSSs <- calcTPM(supportedCTSSs, totalTags="totalTags")
supportedCTSSs <- calcPooled(supportedCTSSs)
```

## 1: Default parameters

### ASCII Representation

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:             [----------]   [----]   [----------------]
```

### R Function

```R
run_clusterUnidirectionally_low_cutoff <- function(ctssObj) {
  clusterUnidirectionally(ctssObj, pooledCutoff=0, mergeDist=20)
}
```

With low `pooledCutoff` and low `mergeDist`, the algorithm is more permissive, allowing for clusters that have varying densities and can span larger regions.

## 2: High `pooledCutoff`

### ASCII Representation

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:                 [---]           [----]       [---]
```

### R Function

```R
run_clusterUnidirectionally_high_cutoff <- function(ctssObj) {
  clusterUnidirectionally(ctssObj, pooledCutoff=0.1, mergeDist=20)
}
```

With a higher `pooledCutoff`, only clusters with more consistent CTSS densities are kept. Clusters with varying densities are filtered out.

## 3: High `mergeDist`

### ASCII Representation

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:                 [--]       [-]   [--] [--]   [-]  [--]
```

### R Function

```R
run_clusterUnidirectionally_high_mergeDist <- function(ctssObj) {
  clusterUnidirectionally(ctssObj, pooledCutoff=0.2, mergeDist=30)
}
```

With a higher `mergeDist`, the algorithm allows for larger gaps between CTSSs, resulting in fewer, more extended clusters.

## 4: Pre-filtered CTSSs

### R Function

```R
run_clusterUnidirectionally_prefiltered <- function(ctssObj) {
  clusterUnidirectionally(ctssObj, pooledCutoff=0, mergeDist=20)
}
```

#### Cluster using defaults: 
```R
clusterUnidirectionally(CTSSs)
```
- **Parameters**: 
- pooledCutoff numeric: Minimum pooled value to be considered as TC.
- mergeDist integer: Merge TCs within this distance.


### Default Parameters
- **Parameters**: `pooledCutoff=0`, `mergeDist=20`
- a baseline clustering for comparison with other parameter settings.

### Custom Pooled Cutoff and Merge Distance 
- **Parameters**: `pooledCutoff=1`, `mergeDist=25`
- **Expectation**: A higher `mergeDist` of 25 allows clusters to span larger genomic regions. 
The `pooledCutoff=1` means that only clusters with a pooled TPM value greater than 1 will be considered. This will result in fewer but broader clusters compared to the default settings.

### High Pooled Cutoff and Low Merge Distance 
- **Parameters**: `pooledCutoff=2`, `mergeDist=10`
- **Expectation**: A higher `pooledCutoff` of 2 will make the clustering more stringent, only considering clusters with a higher pooled TPM value. 
The lower `mergeDist` of 10 will result in smaller clusters. 
- likely get fewer and smaller clusters that are more specific.

### Low Pooled Cutoff and High Merge Distance
- **Parameters**: `pooledCutoff=0.5`, `mergeDist=30`
- **Expectation**: A lower `pooledCutoff` of 0.5 will make the clustering less stringent, allowing clusters with lower pooled TPM values. 
The higher `mergeDist` of 30 will result in broader clusters. 

- likely get more clusters that span larger genomic regions.



## Visualizing in a Genome Browser and TCs PCA
# TCs_C0_02_MD_30_CFR
![PCA - TCs_C0_02_MD_30_CFR](CNS_species_Brain_Terms.png)

