# Paraclu

the results of adjusting how the values of `minStability` and `maxLength` might affect the clustering of CTSSs on a hypothetical genomic region. In the ASCII figures below, each "|" represents a CTSS, and the numbers above them represent the genomic coordinates.

# aggregate TagClusters

Below are ASCII representations to illustrate how different parameter sets for the `aggregateTagClusters` function might affect the consensus clusters. In these figures, each "|" represents a Cap Analysis Gene Expression (CAGE) Transcription Start Site (CTSS), the brackets "[ ]" represent clusters formed by the `clusterCTSS` function, and the braces "{ }" represent consensus clusters formed by the `aggregateTagClusters` function.

## 1: paraclu: Parameters (Low `minStability` and High `maxLength`)
```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:             [----------]   [----]   [----------------]
```

```R

# Function to run clusterCTSS with Default Parameters (Low minStability and High maxLength)
run_clusterCTSS_default <- function(ceCTSS) {
  clusterCTSS(ceCTSS, method = "paraclu", minStability = 1, maxLength = 50)
}
```

With low `minStability` and high `maxLength`, the algorithm is more permissive, allowing for clusters that have varying densities and can span larger regions.

### 1.1 Trial Sets
#### Set 1: High Sensitivity
```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:             [----------]   [----]   [----------------]
Consensus Clusters:   {---------------------------------------}

```

```R
# Parameters (Low `minStability` and High `maxLength`)

run_clusterCTSS_default <- function(ceCTSS) {
  ceCTSS = clusterCTSS(ceCTSS, method = "paraclu", minStability = 1, maxLength = 50)

    # High Sensitivity
    aggregateTagClusters(ceCTSS, tpmThreshold = 1, excludeSignalBelowThreshold = FALSE, maxDist = 200)
}
```
With `tpmThreshold = 1`, `excludeSignalBelowThreshold = FALSE`, and `maxDist = 200`, almost all clusters are included, and larger gaps between clusters are allowed, resulting in a single, large consensus cluster.



#### Set 2: High Specificity

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:             [----------]   [----]   [----------------]
Consensus Clusters:       {---}           {----}       {---}

```
```R
# Parameters (Low `minStability` and High `maxLength`)

run_clusterCTSS_high_stability <- function(ceCTSS) {
  ceCTSS = clusterCTSS(ceCTSS, method = "paraclu", minStability = 1, maxLength = 50)

    # High Specificity
    aggregateTagClusters(ceCTSS, tpmThreshold = 20, excludeSignalBelowThreshold = FALSE, maxDist = 50)
}
```

With `tpmThreshold = 20`, `excludeSignalBelowThreshold = TRUE`, and `maxDist = 50`, only higher TPM clusters are included, and smaller gaps between clusters are allowed, resulting in smaller, more specific consensus clusters.

#### Set 3: Balanced Approach
```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:             [----------]   [----]   [----------------]
Consensus Clusters:       {-------}       {----------}  {-----}

```

```R
# Parameters (Low `minStability` and High `maxLength`)

run_clusterCTSS_high_stability <- function(ceCTSS) {
    ceCTSS = clusterCTSS(ceCTSS, method = "paraclu", minStability = 1, maxLength = 50)

    # Balanced
    aggregateTagClusters(ceCTSS, tpmThreshold = 10, excludeSignalBelowThreshold = FALSE, maxDist = 100)
}
```

With `tpmThreshold = 10`, `excludeSignalBelowThreshold = TRUE`, and `maxDist = 100`, a moderate TPM threshold and gap distance are used, resulting in balanced consensus clusters.

#### 1.2. visualizing them in a genome browser
#### 1.3.TCs pca

## 2: paraclu: Parameters ( High `minStability`)

### High `minStability`

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:                 [---]           [----]       [---]
```
```R
# Function to run clusterCTSS with High minStability
run_clusterCTSS_high_stability <- function(ceCTSS) {
  clusterCTSS(ceCTSS, method = "paraclu", minStability = 3, maxLength = 50)
}


```
With a higher `minStability`, only clusters with more consistent CTSS densities are kept. Clusters with varying densities are filtered out.

### 2.1 Trial Sets
#### Set 1: High Sensitivity

...

#### 2.2. visualizing them in a genome browser
#### 2.3.TCs pca



## 3: paraclu: Parameters (Low `maxLength`)

### Low `maxLength`

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:                 [--]       [-]   [--] [--]   [-]  [--]
```

```R
# Function to run clusterCTSS with Low maxLength
run_clusterCTSS_low_length <- function(ceCTSS) {
  clusterCTSS(ceCTSS, method = "paraclu", minStability = 1, maxLength = 10)
}
```

With a lower `maxLength`, only smaller clusters are allowed. Even if a region has high density, it will be broken into smaller clusters if it exceeds the maximum length.


### 3.1 Trial Sets
#### Set 1: High Sensitivity
...

#### 3.2. visualizing them in a genome browser
#### 3.3.TCs pca

## 4: paraclu: Parameters (High `minStability` and Low `maxLength`)


### High `minStability` and Low `maxLength`

```
Genomic Coordinates: 1   5   10  15  20  25  30  35  40  45  50
CTSS Density:         |   ||| ||  |   |   ||  |||  |   |   ||  |
Clusters:                 [--]           [--]       [--]
```
```R
# Function to run clusterCTSS with High minStability and Low maxLength
run_clusterCTSS_high_stability_low_length <- function(ceCTSS) {
  clusterCTSS(ceCTSS, method = "paraclu", minStability = 3, maxLength = 10)
}

```
With both high `minStability` and low `maxLength`, only small and stable clusters are retained. This is the most stringent setting and will result in fewer, more specific clusters.

### 4.1 Trial Sets
#### Set 1: High Sensitivity
...

#### 3.2. visualizing them in a genome browser
#### 3.3.TCs pca

So on...